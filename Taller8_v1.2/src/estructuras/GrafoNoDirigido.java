package estructuras;


/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * @param K tipo del identificador de los vertices (Comparable)
 * @param E tipo de la informacion asociada a los arcos

 */
public class GrafoNoDirigido<K extends Comparable<K>, E> {

	private final int V;
	private int E;

	/**
	 * Lista de adyacencia 
	 */
	private EstrucBag<Arco<String>>[] adj;

	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
	// Es libre de implementarlo con la representacion de su agrado. 

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido(int pV)
	{
		//TODO implementar
		V = pV;
		E = 0;
		adj = (EstrucBag<Arco<String>>[]) new EstrucBag[V];
		for (int v = 0; v < V; v++)
		{
			adj[v] = new EstrucBag<Arco<String>>();
		}
	}


	public Iterable<Arco<String>> adj(int v) 
	{ 
		return adj[v]; 
	}

	public Iterable<Arco<String>> edges() {
		EstrucBag<Arco<String>> b = new EstrucBag<Arco<String>>();
		for (int v = 0; v < V; v++)
			for (Arco<String> e : adj[v])
				if (e.other(v) > v) b.add(e);
		return b;
	}

	public Arco<String>[] darArcos() 
	{
		int s = 0;

		for(EstrucBag<Arco<String>> x: adj) {
			s += x.size();
		}

		Arco<String>[] array = (Arco<String>[]) new Arco[s];

		int i = 0;
		for(EstrucBag<Arco<String>> x: adj) {
			for(Arco<String> actual : x) {
				array[i] = actual;
				i++;
			}
		}

		return array;
	}

	public int V()
	{  
		return V;
	}

	public int E() {  
		return E; 
	}

	public Arco<String> crearArco(Arco<String> e)
	{
		int v = e.darNodoInicio(), w = e.other(v);
		adj[v].add(e);
		adj[w].add(e);
		E++;

		return e;
	}

}
