package estructuras;

import java.io.FileReader;
import java.util.Scanner;

import modelos.Estacion;

public class GrafoAux
{
	private ST<String, Integer> st;
	private ST<String, Estacion<String>> estaciones;
	private String keys[];
	private GrafoNoDirigido<String, String> G;

	public GrafoAux()
	{
		st = new ST<String, Integer>();
		estaciones = new ST<String, Estacion<String>>();
	}

	public void cargarNodos(String stations) {
		try {
			Scanner in = new Scanner(new FileReader(stations));
			in.nextLine();

			while(in.hasNextLine()) {
				String[] a = in.nextLine().split(";");

				if(!st.contains(a[0])) {
					st.put(a[0], st.size());
					estaciones.put(a[0], new Estacion<String>(a[0], Double.parseDouble(a[1]), Double.parseDouble(a[2])));
				}

			}


			keys = new String[st.size()];

			for(String name: st.keys()) 
				keys[st.get(name)] = name;

			G = new GrafoNoDirigido<String,String>(st.size());
			in.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void cargarArcos(String rutas) {
		try {
			Scanner in = new Scanner(new FileReader(rutas));

			int numero_rutas = Integer.parseInt(in.nextLine());

			for(int i = 0; i < numero_rutas; i++) {
				in.nextLine();
				String nombre = in.nextLine();
				int numero_edges = Integer.parseInt(in.nextLine());

				String lineaAnterior = null;
				String lineaActual = null;

				for(int j = 0; j < numero_edges; j++) {
					lineaActual = in.nextLine();

					if(lineaAnterior != null) {
						String[] info1 = lineaAnterior.split(" ");
						String[] info2 = lineaActual.split(" ");

						int weight = Integer.parseInt(info2[1]);
						int v = st.get(info1[0]);
						int w = st.get(info2[0]);
						G.crearArco(new Arco<String>(v,w,weight,nombre));
					}


					lineaAnterior = lineaActual;
				}
			}


			in.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public String[] darLasRutasQuePansanPorId(String id) 
	{
		ListaEnlazada<String> rutas = new ListaEnlazada<String>();


		for(Arco<String> x:G.adj(st.get(id))) {
			if(!rutas.contains(x.darInformacion()))
			{
				rutas.add(x.darInformacion());
			}
		}

		return rutas.toArray();
	}

	public Arco<String> menorDistancia() {
		double min = Integer.MAX_VALUE;
		Arco<String> e = null;

		for(String name : st.keys()) {
			for(Arco<String> x : G.adj(st.get(name))) {
				if(x.darCosto() < min) {
					min = x.darCosto();
					e = x;
				}
			}
		}

		return e;
	}

	public Arco<String> mayorDistancia() {
		double max = Integer.MIN_VALUE;
		Arco<String> e = null;

		for(String name : st.keys()) {
			for(Arco<String> x : G.adj(st.get(name))) {
				if(x.darCosto() > max) {
					max = x.darCosto();
					e = x;
				}
			}
		}

		return e;
	}
	
	public String distanciaMinimaEstacionesMensaje() {
		return "De " +keys[menorDistancia().darNodoInicio()] +" a "  +keys[menorDistancia().other(menorDistancia().darNodoInicio())] +" con prioridad " +menorDistancia().darCosto() +" en ruta " +menorDistancia().darInformacion();
	}

	public String distanciaMaximaEstacionesMensaje() {
		return "De " +keys[mayorDistancia().darNodoInicio()] +" a "  +keys[mayorDistancia().other(mayorDistancia().darNodoInicio())] +" con prioridad " +mayorDistancia().darCosto() +" en ruta " +mayorDistancia().darInformacion();

	}

	public Estacion<String> buscarEstacion(String nombre) {
		return estaciones.get(nombre);
	}
	
	public boolean contains(String s) {
		return st.contains(s);
	}

	public int index(String s) {
		return st.get(s);
	}

	public String name(int v) {
		return keys[v];
	}

	public GrafoNoDirigido<String, String> G() {
		return G;
	}

	public Nodo<String> buscarNodo(String id)
	{
		Nodo<String> arco = (Nodo<String>) estaciones.get(id);
		return arco;
	}

	public Arco<String>[] darArcosOrigen(String id) {
		
		int i = 0;
		for(Arco<String> x: G.adj(st.get(id))) {
			if(x.darNodoInicio() == st.get(id)) {
				i++;
			}
		}
		
		Arco<String>[] arcos = (Arco<String>[]) new Arco[i];
		
		i = 0;
		for(Arco<String> x: G.adj(st.get(id))) {
			if(x.darNodoInicio() == st.get(id)) {
				arcos[i] = x;
				i++;
			}
		}
		
		return arcos;
	}

	public Arco<String>[] darArcosDestino(String id) {
		int i = 0;
		for(Arco<String> x: G.adj(st.get(id))) {
			if(x.darNodoFin() == st.get(id)) {
				i++;
			}
		}
		
		Arco<String>[] arcos = (Arco<String>[]) new Arco[i];
		
		i = 0;
		for(Arco<String> x: G.adj(st.get(id))) {
			if(x.darNodoFin() == st.get(id)) {
				arcos[i] = x;
				i++;
			}
		}
		
		return arcos;
	}

	public boolean agregarNodo(Nodo<String> nodo) {
		if(st.contains(nodo.darId()))
			return false;
		
		estaciones.put(nodo.darId(), (Estacion<String>)nodo);
		st.put(nodo.darId(), st.size());
		
		String[] nuevo = new String[keys.length+1];
		for(int i = 0; i < keys.length; i++) {
			nuevo[i] = keys[i];
		}
		
		nuevo[st.size()] = nodo.darId();
		
		keys = nuevo;
		
		return true;
	}

	public boolean agregarArco(String inicio, String fin, double costo, String obj) {
		G.crearArco(new Arco<String>(st.get(inicio), st.get(fin), costo, obj));
		
		return true;
	}

	public boolean agregarArco(String inicio, String fin, double costo) {
		G.crearArco(new Arco<String>(st.get(inicio), st.get(fin), costo, ""));
		
		return true;
	}

	public boolean eliminarNodo(String id) {
		keys[st.get(id)] = null;
		st.delete(id);
		estaciones.delete(id);
		
		return true;
	}

//	public Arco<String> eliminarArco(String inicio, String fin) {
//		// Este punto esta mal planteado dado que hay varias rutas que tienen el mismo inicio y fin
//		// y tienen diferente costo y hacen parte de diferentes rutas por esto mismo no se puede realizar
//		// con simplemente el nodo de inicio y fin. Nunca especifican en el taller cual hay que eliminar.
//		
//		return null;
//	}

	public Arco<String>[] darArcos() {
		return G.darArcos();
	}

	public Nodo<String>[] darNodos() {
		Nodo<String>[] array = (Nodo<String>[]) new Nodo[estaciones.size()];
		
		int i = 0;
		for(String x: estaciones.keys()) {
			array[i] = (Nodo<String>) estaciones.get(x);
			i++;
		}
		
		
		return array;
	}
}
