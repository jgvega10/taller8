package estructuras;

import java.util.Iterator;


public class ListaEnlazada<Item> implements  Iterable<Item>
{
	private Node<Item> first;
	private int N;

	public ListaEnlazada() {
		//
		N = 0;
		first = null;
	}

	public void add(Item item) {
		if(first == null) {
			first = new Node<Item>(item);
		} else {
			Node<Item> current = first;

			while(current.getNext() != null) {
				current = current.getNext();
			}

			current.setNext(new Node<Item>(item));
		}

		N++;
	}

	public Item get(int index) {		
		Item ans = null;

		if(index < 0 || index >= N) {
			return ans;
		}
		else if(!isEmpty()) {
			int i = 0;
			Node<Item> current = first;

			while(i != index) {
				current = current.getNext();
				i++;
			}

			ans = current.getItem();
		}

		return ans;
	}

	public Node<Item> getFirst() {
		return first;
	}

	public void remove(int index) {

		if(index < 0 || index >= N) {
			return;
		} else if(index == 0) {
			first = first.getNext();
			N--;
		}
		else if(!isEmpty()) {
			int i = 0;
			Node<Item> current = first;

			while(i+1 != index) {
				current = current.getNext();
				i++;
			}

			current.setNext(current.getNext().getNext());
			N--;
		}
		
	}

	public boolean isEmpty() {
		return first == null;
	}

	public int size() {
		return N;
	}
	
	public boolean contains(Item item) {
		for(Node<Item> current = first; current != null; current = current.next) {
			if(current.item.equals(item))
				return true;
		}
		
		return false;
	}
	
	public String[] toArray() {
		String[] array = new String[N];
		
		int i = 0;
		for(Node<Item> current = first; current != null; current = current.next) {
			array[i] = current.item.toString();
			i++;
		}
		
		return array;
	}
	
	// Iterator
	
	public Iterator<Item> iterator() {
		return new ListIterator();
	}
	
	// Private classes
	
	private class ListIterator implements Iterator<Item> {
		Node<Item> current = first;
		
		public boolean hasNext() {
			return current != null;
		}
		
		public Item next() {
			Item item = current.getItem();
			current = current.getNext();
			return item;
		}
		
		public void remove() {}
	}
	
	private class Node<Item> {
		
		Item item;
		Node<Item> next;
		
		public Node(Item item) {
			this.item = item;
		}
		
		public void setNext(Node<Item> next) {
			this.next = next;
		}
		
		public Item getItem() {
			return item;
		}
		
		public Node<Item> getNext() {
			return next;
		}
	}
}
