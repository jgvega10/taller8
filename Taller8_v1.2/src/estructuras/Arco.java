package estructuras;

public class Arco<E> implements Comparable <Arco<E>> 
{

	/**
	 * Costo de ir de nodo inicio a nodo fin
	 */
	private double costo;

	/**
	 * Informacion adicional que se puede guardar en el arco
	 */
	//actualizacion
	private E obj;

	/**
	 * Nodo inicio 
	 */
	private int inicio;

	/**
	 * Nodo fin
	 */
	private int fin;


	/**
	 * Construye un nuevo arco desde un nodo inicio hasta un nodo fin
	 * con un peso dado e información adicional. 
	 * @param inicio el nodo inicial del arco
	 * @param fin el nodo final del arco
	 * @param costo Costo del arco
	 * @param obj Información adicional que se desea guardar
	 */
	public Arco(int inicio, int fin, double costo, E obj) {
		this.costo = costo;
		this.obj = obj;
		this.inicio = inicio;
		this.fin = fin;
	}

	/**
	 * Construye un nuevo arco desde un nodo inicio hasta un nodo fin.
	 * @param inicio Nodo inicial del arco.
	 * @param fin Nodo final del arco.
	 * @param costo Costo del arco. 
	 */
	public Arco(int inicio, int fin, double costo) {
		this.costo = costo;
		this.inicio = inicio;
		this.fin = fin;
	}


	/**
	 * Devuelve el nodo inicio del arco
	 * @return Nodo inicio
	 */
	public int darNodoInicio() {
		return inicio;
	}

	/**
	 * Devuelve el nodo final del arco
	 * @return Nodo fin
	 */
	public int darNodoFin() 
	{
		return fin;
	}

	/**
	 * Devuelve el costo del arco
	 * @return costo
	 */
	public double darCosto() {
		return costo;
	}

	/**
	 * Asigna un objeto como informacion adicional asociada al arco
	 * @param info Objeto (tipo E) que se desea guardar como información adicional
	 * @return este arco con la información adicional asignada
	 */
	public  Arco<E> asignarInformacion(E info) {
		obj = info; 
		return this;
	}

	public int other(int vertex) {
		if      (vertex == inicio) return fin;
		else if (vertex == fin) return inicio;
		else return -1;
	}
	/**
	 * Devuelve la información adicional asociada al arco
	 * @return objeto (tipo E) asociado como información adicional
	 */
	public 	E darInformacion() {
		return obj;
	}
	public int compareTo(Arco<E> that) {
		if(this.darCosto() < that.darCosto())
		{
			return -1;
		}
		else if (this.darCosto() > that.darCosto())
		{
			return +1;
		}
		else{
			return  0;
		}
	}
}
