 package mundo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Set;
import java.util.TreeSet;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Comparison;

import modelos.Estacion;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoAux;
import estructuras.GrafoNoDirigido;
import estructuras.Nodo;

/**
 * Clase que representa el administrador del sistema de metro de New York
 *
 */
public class Administrador
{

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";
	
	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";

	
	/**
	 * Grafo que modela el sistema de metro de New York
	 */
	//TODO Declare el atributo grafo No dirigido que va a modelar el sistema. 
	// Los identificadores de las estaciones son String
	private GrafoAux grafo;

	/**
	 * Construye un nuevo administrador del Sistema
	 */
	public Administrador() 
	{
		grafo = new GrafoAux();
		//TODO inicialice el grafo como un GrafoNoDirigido
	}

	public GrafoAux darGrafo()
	{
		return grafo;
	}
	/**
	 * Devuelve todas las rutas que pasan por la estacion con nombre dado
	 * @param identificador 
	 * @return Arreglo con los identificadores de las rutas que pasan por la estacion requerida
	 */
	public String[] darRutasEstacion(String identificador)
	{
		//TODO Implementar
		String[] res = grafo.darLasRutasQuePansanPorId(identificador); 
		return res;
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones mas cercanas del sistema.
	 * @return distancia minima entre 2 estaciones
	 */
	public double distanciaMinimaEstaciones()
	{
		//TODO Implementar
		double res = grafo.menorDistancia().darCosto();
		return res;
	}
	
	/**
	 * Devuelve la distancia que hay entre las estaciones más lejanas del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public double distanciaMaximaEstaciones()
	{
		//TODO Implementar
		double res = grafo.mayorDistancia().darCosto();
		return res;
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		//TODO Implementar
		grafo.cargarNodos(RUTA_PARADEROS);
		
		System.out.println("Se han cargado correctamente "+grafo.darNodos().length+" Estaciones");
		
		//TODO Implementar
		grafo.cargarArcos(RUTA_RUTAS);
		
		System.out.println("Se han cargado correctamente "+ grafo.darArcos().length+" arcos");
	}
	
	/**
	 * Busca la estacion con identificador dado<br>
	 * @param identificador de la estacion buscada
	 * @return estacion cuyo identificador coincide con el parametro, null de lo contrario
	 */
	public Estacion<String> buscarEstacion(String identificador)
	{
		//TODO Implementar
		Estacion res = grafo.buscarEstacion(identificador);
		return res;
	}

}
