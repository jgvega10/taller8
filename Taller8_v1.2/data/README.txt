* Para que sirve el HashMap adj?

- Un HashMap es muy util dado que para buscar un elemento que pertenece al grafo solo hay que conocer su llave, en este caso 
para las estaciones su llave era el id ya que este es unico para cada una de ellas. Ademas de la llave, este tambien puede 
contener su valor, por lo tanto la complejidad sera constante en el mejor caso, ya que las llaves son unicas y se puede
encontrar en tiempo constante.